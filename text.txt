The way you move is like a full on rainstorm
And I am a house of cards
You are the kind of reckless that should send me running
But I kinda know that I will not get far

And you stood there in front of me just
Close enough to touch
Close enough to hope you could not see
What I was thinking of

Drop everything now
Meet me in the pouring rain
Kiss me on the sidewalk
Take away the pain
Cause I see sparks fly whenever you smile

Get me with those green eyes baby
As the lights go down
Gimme something that will haunt me whenever you are not around
Cause I see sparks fly when you smile

My mind forgets to remind me your a bad idea
You touch me once and it is really something
You find I am even better than you imagined I would be
I am on my guard for the rest of the world
But with you I know it is no good
And I could wait patiently
But I really wish you would

Drop everything now
Meet me in the pouring rain
Kiss me on the sidewalk
Take away the pain
Cause I see sparks fly whenever you smile

Get me with those green eyes baby
As the lights go down
Gimme something that will haunt me when you are not around
Cause I see sparks fly whenever you smile

I run my fingers through your hair
And watch the lights go wild
Just keep on keeping your eyes on me
It is just wrong enough to make it feel right
And lead me up the staircase
Will not you whisper soft and slow
And I am captivated by you baby
Like a fireworks show

Drop everything now
Meet me in the pouring rain
Kiss me on the sidewalk
Take away the pain
Cause I see sparks fly whenever you smile

Get me with those green eyes baby
As the lights go down
Gimme something that will haunt me when you are not around
Cause I see sparks fly whenever you smile

When sparks fly oh baby smile
When sparks fly