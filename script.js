
/**
* Map function 
* @param {string} line : string which contains a whole line 
* @return {array} toReturn : array of arrays which contain a word with a value 1 each
*/
function map(line){
	//if the line is empty, do nothing
	if (line ==''){
		return [];
	}

	// split the line into array which contain each word from the line 
	var tab = line.split(" ");

	var toReturn = [];
	// for each word in the "tab" array 
	for(var i in tab){
		var tmp = [];
		tmp = [tab[i],1];  // create an array with the word and the value 1 
		toReturn.push(tmp); // add this previous array to the "toReturn" array, which contains the arrays for each word
	}
	return toReturn;
}

/**
* Shuffle and Sort
* This function below do the sort and the shuffle  
* @param {array} maps, this array contains arrays of each words with their value 1
* @return {array} shuf, contain arrays of word with an array of ones.
*/
function shuffle(maps){

	//sort the array by word in the alphabetic order
	maps.sort();
	
	// shuffle 
	var shuf = []; 

	// for each array (word, 1) of maps array
    for(var m in maps){
    	// if there is nothing in the "shuf" array, 
    	// we add the first array from "maps" to "shuf" array (which will be returned) 
		if (shuf.length===0){
    		var tmp = [maps[m][0], [maps[m][1]]] // we transform the array to (word, [1])
    		shuf.push(tmp);
    	} else if (shuf[shuf.length-1][0] === maps[m][0]) {  // if the word is the same than the last one in shuf,
    		shuf[shuf.length-1][1].push(1); //we add a 1 to the array, for instance (word, [1,1]) or (word, [1,1,1]), etc. 
    	} else { // if the word is different, we add a new array to "push" array
    		var tmp = [maps[m][0], [maps[m][1]]] 
    		shuf.push(tmp);
    	}
    }
    return shuf;
}

/**
 * Reduce 
 * @param {array} toReduce, an array with the word and an array of ones  
 * @return {array} r, an array with the word and its occurence
 */
function reduce(toReduce){
	var count =0;
	// for a word, count the number of ones in the array 
 	for (var t in toReduce[1]){
 	 	count++;
 	}
	var r = [toReduce[0], count]; //create an array with the word and the occurence of the word
	return r;
}

/**
 * Output 
 * @param  {array} reduced, which contains the arrays of the words and their occurences
 * @return {string} output, transforms the array to string in order to display the results
 */
function out(reduced){
	var output="";
	for (var w in reduced){
		output += reduced[w][0] + " " + reduced[w][1] + "</br>";
	}
	return output;
}

/**
* onLoaded  
* This function will implement the Map/Reduce algorithm
* @param {string} source, the text that we want to apply the WordCount is stored in this string 
*/
function onLoaded(source){
 	 // We convert the whole text in lower case 
 	 source = source.toLowerCase();

	 // we split the text to get each line in an array
	 var lines = source.split('\n');
	 
	 // Map 
	 var maps = [];
	 // for each line, we call the map function 
	 // the map function returns an array
	 // we concat this array with the results of the other line mapper.
	 for (var l in lines){
	 	maps = maps.concat(map(lines[l]));
	 }

	 // Shuffle/sort
	 // the arrays of words are sorted in alphabetic order 
	 // the same words are shuffled into an unique array with the word and an array of ones
	 var shuf = [];
	 shuf = shuffle(maps);

	 // Reduce 
	 // for each array of word and its array of ones 
	 // the reduce function returns an array with the word and its occurence
	 var reduced = [];
 	 for (var word in shuf){
 		reduced.push(reduce(shuf[word])); // each reduced array is included in an array 
 	 }
 
	 // Output
	 // The results of the reduce are converted to string and displayed 
	 var output = out(reduced);
	 $(".main").html(output);

}

$(document).ready(function(){
	$.get("text.txt", onLoaded); // the text to WordCount is read from text.txt
});